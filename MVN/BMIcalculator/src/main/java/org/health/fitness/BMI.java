
package org.health.fitness;


public class BMI {
  private int height;
  private int weight;
  
  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    if(height<140 || height>220){
      throw new IllegalArgumentException("Your height must be between 140 and 220 centimeters!");
    }
    this.height = height;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    if(weight<40 || weight>160){
      throw new IllegalArgumentException("Your weight must be between 40 and 160 kilogramms!");
    }
    this.weight = weight;
  } 
  
  
}
