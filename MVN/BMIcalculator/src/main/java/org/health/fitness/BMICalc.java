
package org.health.fitness;

import java.util.ArrayList;

public class BMICalc {
  
  BMI bmi=new BMI();
  
  int ht;
  int wt;
  double bmiindex;
  ArrayList<String> message=new ArrayList<String>();
  String[] category={"Severe Thinness", "Moderate Thinness","Mild Thinness", 
    "Normal", "Overweight", "Obese Class I", "Obese Class II", "Obese Class III"};
  
  public void calculate(){
    
    bmi.setHeight(ht);  //default:cm
    bmi.setWeight(wt);  //default:kg
    
    bmiindex=(bmi.getWeight()/(Math.pow((double)bmi.getHeight()/100,2)));
    String indextostring=String.valueOf(bmiindex);
    
    if(bmiindex<16){
      message.add(indextostring);
      message.add(category[0]);}
    else if(bmiindex<17){
      message.add(indextostring);
      message.add(category[1]);}
    else if(bmiindex<18.5){
      message.add(indextostring);
      message.add(category[2]);}
    else if(bmiindex<25){
      message.add(indextostring);
      message.add(category[3]);}
    else if(bmiindex<30){
      message.add(indextostring);
      message.add(category[4]);}
    else if(bmiindex<35){
      message.add(indextostring);
      message.add(category[5]);}
    else if(bmiindex<40){
      message.add(indextostring);
      message.add(category[6]);}
    else{
      message.add(indextostring);
      message.add(category[7]);}
  }
  
  //f�ggv�nyek a metrikusb�l val� �tv�lt�sra
  
  public double getWeightinlbs(){
    bmi.setWeight(wt);
    int x=(bmi.getWeight());              //kg=lb/2,2046
    double lbs=((double)x*2.2046);
    return lbs;
  }
  
  public double getHeightinfeet(){
    bmi.setHeight(ht);
    int x=(bmi.getHeight());            //1 l�b=30,48cm
    double ft=((double)x/30.48);
    return ft;
  }
  
  public double getHeightininches(){
    bmi.setHeight(ht);
    int x=(bmi.getHeight());            //1 inch=2,54cm
    double in=((double)bmi.getHeight()/2.54);
    return in;
  }
}
