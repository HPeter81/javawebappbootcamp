
package org.health.fitness;

import java.util.ArrayList;
import junit.framework.Assert;
import static junit.framework.Assert.fail;
import org.junit.Test;


public class BMICalcTest {
  
  
  @Test(expected=IllegalArgumentException.class)
  public void testHeighttMustNotBeAcceptedWhenOutOfRange1(){
   BMI instance=new BMI();
   int ht=100;
     instance.setHeight(ht);
   
   }
   
  
  
  @Test(expected=IllegalArgumentException.class)
  public void testHeighttMustNotBeAcceptedWhenOutOfRange2(){
    BMI instance=new BMI();
   int ht=300;
     instance.setHeight(ht);
   
   }
  
  
  @Test(expected=IllegalArgumentException.class)
  public void testWeightMustNotBeAcceptedWhenOutOfRange1(){
    BMI instance=new BMI();
    int wt=10;
    
      instance.setWeight(wt);
    
  }
  
 @Test(expected=IllegalArgumentException.class)
 public void testWeightMustNotBeAcceptedWhenOutOfRange2(){
   BMI instance=new BMI();
   int wt=300;
   
     instance.setWeight(wt);
   
 }
 
 @Test
 public void testCalculateBmiIsOK(){
   BMI instance=new BMI();
   int ht=180;
   int wt=80;
   double bmiindex;
   instance.setHeight(ht);
   instance.setWeight(wt);
   
   bmiindex=(instance.getWeight()/(Math.pow((double)instance.getHeight()/100,2)));
   
  if(bmiindex>23 && bmiindex<25)
     System.out.println("OK");
  else
    fail("Test failed");
  
 }
 
 @Test
 public void testCategoryIsOk(){
   BMI instance=new BMI();
   int ht=180;
   int wt=80;
   double bmiindex;
   instance.setHeight(ht);
   instance.setWeight(wt);
   String[] category={"Severe Thinness", "Moderate Thinness","Mild Thinness", 
    "Normal", "Overweight", "Obese Class I", "Obese Class II", "Obese Class III"};
  ArrayList<String> messagetest=new ArrayList<String>();
  
  ArrayList<String> expected=new ArrayList();
  expected.add("24.691358024691358");
  expected.add("Normal");
  
   bmiindex=(instance.getWeight()/(Math.pow((double)instance.getHeight()/100,2)));
   
   if(bmiindex<25){
     String indextostring=String.valueOf(bmiindex);
     messagetest.add(indextostring);
     messagetest.add(category[3]);
   }
   
   
   Assert.assertEquals(messagetest.get(0), expected.get(0));
   Assert.assertEquals(messagetest.get(1), expected.get(1));
 }
 
 @Test
 public void testGetWeightInLbs(){
   BMI bmi=new BMI();
   
   int wt=80;
   bmi.setWeight(wt);
   int x=(bmi.getWeight());
    double lbs = ((double)x*2.2046);
    Assert.assertEquals(176.368, lbs);
   
 }
 
 @Test
 public void testGetHeightInFeet(){
   BMI bmi=new BMI();
   
   int ht=180;
   bmi.setHeight(ht);
   int x=(bmi.getHeight());
    double ft=((double)x/30.48);
   Assert.assertEquals(5.905511811023622, ft);
 }
 
 @Test
 public void testGetHeightInInches(){
   BMI bmi=new BMI();
   
   int ht=180;
   bmi.setHeight(ht);
   int x=(bmi.getHeight());
    double in=((double)x/2.54);
    Assert.assertEquals(70.86614173228347, in);
 }
}
